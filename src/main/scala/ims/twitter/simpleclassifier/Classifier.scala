package ims.twitter.simpleclassifier

import java.io._

import de.bwaldvogel.liblinear._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import scala.util.Random

/**
 * Created by rklinger on 7/30/16.
 */
class Classifier(myModel:MyModel) {
  def getModel:MyModel = myModel
  Linear.setDebugOutput(System.err)
}

/**
 * A simple tokenizer.
 */
object Tokenizer {
  def simpleTokenize(s: String): Array[String] = {
    import java.util.Scanner
    val preprocessingString = s.replaceAll(" +"," ").replaceAll("[^#A-Za-z0-9_= ]","")
    val scanner = new Scanner(preprocessingString)
    var r = new ArrayBuffer[String];
    try {
      while (scanner.hasNext()) {
        scanner.findInLine("[#a-zA-Z0-9_=]+|[^ ]")
          val result = scanner.`match`()
          r += result.group()
        }
      } catch {
      case e: Exception => {
        System.err.println("Tokenization error for " + preprocessingString)
      }
    }
    r.toArray
  }
}

class MalformedTextInstance extends TextInstance("","",null)

/**
 * One Tweet or sentence. Might be used to represent more than just one sentence, because it does not not know about such linguistic structures. 
 * Holds features and can convert itself to liblinear data structures.
 * @param text
 * @param goldLabel
 * @param em
 */
class TextInstance(var text:String,goldLabel:String,em:MyModel) {
  private var predictedLabel = "n/a"
  private var rankedLabels:List[(String,Double)] = List[(String,Double)]()
  private var featureIds:mutable.HashSet[Int] = new mutable.HashSet[Int]()
  initFeatureStructure // important
  
  def resultString = getPredictedLabel+"\t"+getGoldLabel+"\t"+text
  def rankedResultString = getPredictedLabel+"\t"+getGoldLabel+"\t"+text+"\t"+rankedLabels.map(x=>x._1+":"+"%1.4f".format(x._2)).mkString("\t")

  private def initFeatureStructure = {
    val tokens = Tokenizer.simpleTokenize(text).toBuffer[String].map(x => x.replaceAll("@[A-Za-z0-9]","@USERNAME"))
    //text = null // more space efficient in training, but nicer to keep this when doing prediction
    val features = new ArrayBuffer[String]
    features ++= tokens.map(t => "W="+t) // 1-grams
    features ++= (for(i <- 0 until tokens.size-1) yield "2G="+tokens(i)+"_"+tokens(i+1)) // 2-grams
    features.map(feature => em.voc2fId(feature)).toSet.filter(x => x != -1).foreach(x => featureIds.add(x))
  }

  def csvString : String = {
    val labelASID = em.label2Id(getGoldLabel).toInt
    val featuresAsCSV = (0 until em.featureSize).map(i => if (featureIds.contains(i)) 1 else 0).mkString(",")
    labelASID + "," + featuresAsCSV
  }

  def setPredictedLabel(label:String) = {predictedLabel=label}
  
  def setRankedLabels(rankedLabels:List[(String,Double)]) = {this.rankedLabels=rankedLabels}
  
  def getPredictedLabel = predictedLabel
  def getGoldLabel = goldLabel
  
  def toFeatureArray : Array[Feature] = {
     val featureIdsSorted = featureIds.toList.sortBy(x => x.toDouble)
     val featureNodes = featureIdsSorted.map(id => new FeatureNode(id, 1.0)).toArray[Feature]
     featureNodes
  }
  def toLabelId(em:MyModel) : Double = {
    em.label2Id(goldLabel)
  }
}
object TextInstance {
   def fromCSVLine(csvLine:String,em:MyModel,ignoreLabel:Boolean,debugLabel:String="debugLabel") : TextInstance = {
      val split = csvLine.split("\\t")
      if (split.length < 2) {
        System.err.println("Line not well-formed "+csvLine)
        new MalformedTextInstance
      } else {
        val label = if (ignoreLabel) "N/A" else if (debugLabel!="debugLabel") debugLabel else split(0)
        new TextInstance(split(1),label, em)
      }
   }
}

/**
 * A set of results. Can print itself. *
 */
class ResultSet {
  val results = new ArrayBuffer[Result]
  def macroF1 = if (results.size.toDouble==0) 0.0 else results.map(_.f1).sum/results.size.toDouble
  def printResults(outStream:PrintStream = System.err) {
    outStream.println("Label     TP\tFP\tFN\tP\tR\tF1")
    results.foreach(outStream.println(_))
    val (p,r,f) = macroResults
    val macroString = "Macro-Av.".padTo(10," ").mkString("")+"\t\t\t\t"+Classifier.floatprint(p)+"\t"+Classifier.floatprint(r)+"\t"+Classifier.floatprint(f)
    outStream.println(macroString)
  }
  def macroResults : (Double,Double,Double) = {
    val resultsWithoutMicroAverage = results.filter(x => x.label != "Micro-Average")
    val p:Double = resultsWithoutMicroAverage.map(x => x.precision).sum / resultsWithoutMicroAverage.size.toDouble
    val r:Double = resultsWithoutMicroAverage.map(x => x.recall).sum / resultsWithoutMicroAverage.size.toDouble
    val f:Double = resultsWithoutMicroAverage.map(x => x.f1).sum / resultsWithoutMicroAverage.size.toDouble
    (p,r,f)
  }
}

/**
 * Evaluation result. * 
 * @param label
 */
class Result(val label:String) {
  private var tp:Int = 0
  private var fp:Int = 0
  private var fn:Int = 0
  private val instanceWiseTPFPFN = new ArrayBuffer[(Int,Int,Int)]
  def getInstanceWiseTPFPFN = instanceWiseTPFPFN
  def addToInstanceWiseTPFPFN(tpfpfnList:ArrayBuffer[(Int,Int,Int)]) = this.instanceWiseTPFPFN ++= tpfpfnList
  def incFP = {fp += 1}
  def incFN = {fn += 1}
  def incTP = {tp += 1}
  def recall = if (tp+fn!=0) (tp.toDouble / (tp.toDouble + fn.toDouble)) else 0.0
  def precision = if (tp+fp!=0) (tp.toDouble / (tp.toDouble + fp.toDouble)) else 0.0
  def f1 = if ((precision+recall)==0.0) 0.0 else (2*recall*precision)/(precision+recall)
  override def toString() : String = {
    label.padTo(10," ").mkString("")+"\t"+tp+"\t"+fp+"\t"+fn+"\t"+Classifier.floatprint(precision)+"\t"+Classifier.floatprint(recall)+"\t"+Classifier.floatprint(f1)
  }
  def setFP(fp:Int) = this.fp = fp
  def setTP(tp:Int) = this.tp = tp
  def setFN(fn:Int) = this.fn = fn
  def getFP = fp
  def getTP = tp
  def getFN = fn
}

/**
 * The corpus of texts. *
 * @param textInstances
 */
class Corpus(private var textInstances:ArrayBuffer[TextInstance],em:MyModel) {
  def getTextInstances = textInstances
  def getAllLabels = textInstances.map(_.getGoldLabel).toSet
  def size = textInstances.size
  def toFeatureArray : Array[Array[Feature]] = {
    textInstances.map(instance => instance.toFeatureArray).toArray
  }
  def toLabels : Array[Double] = {
    textInstances.map(instance => instance.toLabelId(em)).toArray
  }
  def toProblem(em:MyModel) : Problem = {
    val problem = new Problem();
    problem.l = this.size // number of training examples
    problem.n = em.featureSize // number of features
    problem.x = this.toFeatureArray // feature nodes
    problem.y = this.toLabels // target values
    problem
  }

  def subsample(fraction:Double) : Corpus = {
    val newInstances = textInstances.filter(ti => em.getRandom.nextDouble() < fraction)
    new Corpus(newInstances,em)
  }
  
  def evaluate : ResultSet = {
    val resultSet = new ResultSet
    val r = new mutable.HashMap[String,Result]()
    // generate Result objects for each label
    val allLabels = (textInstances.map(_.getGoldLabel) ++ textInstances.map(_.getPredictedLabel)).toSet
    //System.err.println("Evaluating for the following labels:\n"+allLabels.mkString("\t","\n","\n"))
    allLabels.foreach((x:String) => r += x -> new Result(x))

    // actual evaluation
    for(instance <- textInstances) {
      if (instance.getPredictedLabel == instance.getGoldLabel) {
        r(instance.getGoldLabel).incTP
      }
      else {
        r(instance.getGoldLabel).incFN
        r(instance.getPredictedLabel).incFP
      }
    }

    // add one for each class
    resultSet.results ++= r.values
    // calculate micro average
    val microaverageResult = new Result("Micro-Average")
    microaverageResult.setFN(r.values.map(x => x.getFN).sum)
    microaverageResult.setFP(r.values.map(x => x.getFP).sum)
    microaverageResult.setTP(r.values.map(x => x.getTP).sum)
    r.values.foreach(x => microaverageResult.addToInstanceWiseTPFPFN(x.getInstanceWiseTPFPFN))

    resultSet.results += microaverageResult
    
    resultSet
  }

  def printResults(ps:PrintStream) {
    for(ti <- textInstances) {
      ps.println(ti.rankedResultString)
    }
  }

  /**
   * first column label, then features *
   * @param outputfile
   */
  def writeToCSVFile(outputfile:String) {
    val file = new File(outputfile)
    val bw = new BufferedWriter(new FileWriter(file))
    textInstances.foreach(x => bw.write(x.csvString+"\n"))
    bw.close()
  }
  


}


object Corpus {
  def readFromFile(tweetCSVFile:File,em:MyModel,take:Int=Integer.MAX_VALUE,ignoreLabel:Boolean=false,debugLabel:String="debugLabel") : Corpus = {
    System.err.println("Reading corpus with BOW")
    if (!tweetCSVFile.exists()) {
      System.err.println("File does not exist: "+tweetCSVFile.getAbsolutePath)
      System.exit(-1)
    }
    System.err.println("Reading from "+tweetCSVFile.getAbsolutePath)
    val source = Source.fromFile(tweetCSVFile)
    val instances = new ArrayBuffer[TextInstance]
    for(tweet <- source.getLines().take(take)) {
      val ti = TextInstance.fromCSVLine(tweet,em,ignoreLabel,debugLabel)
      if (!ti.isInstanceOf[MalformedTextInstance]) {
        instances += ti
      }
    }
    val corpus = new Corpus(instances, em)

    System.err.println("Read "+corpus.size+" Tweets.")
    corpus
  }
}

/**
 * Represents all data structures of the ML model and other necessary data structures. *
 */
@SerialVersionUID(1)
class MyModel extends Serializable {
  private var random = new Random(0)
  private var liblinearmodel = new Model
  private val vocabularyIdMapper = new mutable.HashMap[String,Int]
  private val idVocabularyMapper = new mutable.HashMap[Int,String]
  private val labelIdMapper = new mutable.HashMap[String,Double]
  private val idLabelMapper = new mutable.HashMap[Double,String]
  private var vocabularyFixed = false
  private var lastFeatureId = 0
  private val tiSubsetSize = 20000
  private var idfmap = new mutable.HashMap[Int,Double]
  def addToIdfMap(featureId:Int,idfWeight:Double) = {
    if (!vocabularyFixed) {
      idfmap += featureId -> idfWeight
    }
  }

  def getLabels : List[String] = labelIdMapper.keys.toList.sortBy(_.hashCode)
  
  def fixVoc = {vocabularyFixed = true}
  def resetVocabulary = { vocabularyIdMapper.clear() ; idVocabularyMapper.clear() ; }
  
  def getAllFeatureIDs = vocabularyIdMapper.values

  def getRandom = random
  
  def featureId2Voc(id:Int) : String = if (idVocabularyMapper.contains(id)) idVocabularyMapper(id) else "UNKNOWN"
  
  def voc2fId(token:String) : Int = {
    if (vocabularyIdMapper.contains(token)) {
      vocabularyIdMapper(token)
    } else if (isVocabularyFixed) {
      -1
    } else {
      lastFeatureId += 1
      vocabularyIdMapper += token -> (lastFeatureId)
      lastFeatureId
    }
  }
  def label2Id(label:String) : Double = if(labelIdMapper.contains(label)) labelIdMapper(label) else -1
  def id2Label(index:Int) : String = if(idLabelMapper.contains(index)) idLabelMapper(index) else "UNKNOWN"
  def isVocabularyFixed = vocabularyFixed
  def buildVocabularyMapper(corpus:Corpus) = {
    if (isVocabularyFixed) {
      throw new Exception("Vocabulary fixed!")
    }
    System.err.println("Analyzing vocabulary...")
    System.err.println("Found "+vocabularyIdMapper.keySet.size+" different features.")
  }
  def buildLabelMapper(corpus:Corpus) = {
    if (vocabularyFixed) {
      throw new Exception("Vocabulary fixed!")
    }
    System.err.println("Analyzing labels...")
    val allLabels = corpus.getAllLabels
    System.err.println("Found "+allLabels.size+" different labels:")
    System.err.println(allLabels.mkString(" ; "))
    var i = -1.0
    for(aLabel <- allLabels) {
      i += 1.0
      labelIdMapper += aLabel -> i
      idLabelMapper += i -> aLabel
    }
  }
  def featureSize = vocabularyIdMapper.keySet.size
  def labelSize = labelIdMapper.keySet.size

  /**
   * Train Model.
   * @param corpus
   * @return
   */
  def train(corpus:Corpus) : Model = {
    System.err.println("Initializing the learning...")
//    val solver = SolverType.L2R_L2LOSS_SVC
    val solver = SolverType.L2R_LR
    val C = 1.0;    // cost of constraints violation
    val eps = 0.01; // stopping criteria
    val parameter = new Parameter(solver, C, eps);
    System.err.println("Converting the data to sparse representations...")
    val problem = corpus.toProblem(this)
    System.err.println("Starting to learn...")

    liblinearmodel = Linear.train(problem, parameter);
    System.err.println("Done.")
    liblinearmodel
  }

  /**
   * Predict labels for a whole corpus. * 
   * @param corpus
   */
  def predict(corpus:Corpus) {
    for(instance <- corpus.getTextInstances) {
      val features = instance.toFeatureArray
      //val result = Linear.predict(liblinearmodel,features)
      val ar = new Array[Double](labelSize)
      val result = Linear.predictProbability(liblinearmodel,features,ar)
      val rankedLabels = labelRank(ar)
      instance.setPredictedLabel(idLabelMapper(result))
      instance.setRankedLabels(rankedLabels)
    }
  }
  
  def arrayArgmax(probabilities:Array[Double]) : Int = {
    //val l = probabilities.toList ; l.indexOf(l.max) // slot
    probabilities.toList.view.zipWithIndex.maxBy(_._1)._2
  }
  
  def labelRank(probabilities:Array[Double]) : List[(String,Double)] = {
    val r = probabilities.toList.zipWithIndex.sortBy(-1* _._1) // first position: value, second position: index // sorting not used later
    val r2 = r.map(x => (liblinearmodel.getLabels()(x._2),x._1)) // map to label index, no clue why this is needed; then turn around
    val r3 = r2.sortBy(_._1) // sort by emotion id
    r3.map(x => (id2Label(x._1),x._2)) // to string -> value
  }

  /**
   * Predict label for just one text. Nice for interactive use or pipeing. *
   * @param text
   */
  def predictText(text:String,id:String,ps:PrintStream) {
    val ti = new TextInstance(text,"n/a",this)
    val features = ti.toFeatureArray
    val ar = new Array[Double](labelSize)
    val result = Linear.predictProbability(liblinearmodel,features,ar)
    val rankedLabels = labelRank(ar)
    ti.setPredictedLabel(idLabelMapper(result))
    ti.setRankedLabels(rankedLabels)
    ps.println(ti.rankedResultString)
  }
  
  def save(fileName:String) {
    System.err.println("Saving the model to: "+fileName)
    val file = new File(fileName) 
    if (!file.getParentFile.canWrite) {
      System.err.println("Sorry, I cannot write there! Does the folder exist?")
    } else {
      val oos = new ObjectOutputStream(new FileOutputStream(fileName))
      oos.writeObject(this)
      oos.flush
      oos.close
      System.err.println("Done.")
    }
  }
}
object MyModel {
  def load(fileName:String) : MyModel = {
    val file = new File(fileName)
    System.err.println("Reading from "+file.getAbsolutePath)
    if (!file.canRead) {
      System.err.println("I cannot read this!")
      System.exit(1)
      new MyModel // fake
    } else {
      val ois = new ObjectInputStream(new FileInputStream(file))
      val em = ois.readObject
      em.asInstanceOf[MyModel]
    }
  }
}


object Classifier {
  def floatprint(f:Double) : String = (f*100).round.toString
  
  def main(args:Array[String]) {

    System.err.println("Simple Text Classifier 0.1")
    System.err.println("klinger@ims.uni-stuttgart.de")
    
    // argument parsing
    var trainFileName = ""
    var testFileName = ""
    var modelFileName = ""
    var takeArg = ""
    var pmi = ""
    var trainQeThreshold = Double.NegativeInfinity
    args.sliding(2, 1).toList.collect {
      case Array("--train", x: String) => trainFileName = x
      case Array("--test", x: String) => testFileName = x
      case Array("--model", x: String) => modelFileName = x
      case Array("--take", x: String) => takeArg = x
    }
    val take = if (takeArg.isEmpty) Int.MaxValue else takeArg.toInt

    if (trainFileName.isEmpty && modelFileName.isEmpty) {
      System.err.println("Specify appropriate parameters!" +
        "Examples are:\n" +
        "--train FILENAME1 --test FILENAME2 (trains on FILENAME1 and tests on FILENAM2)\n" +
        "--train FILENAME --model SAVEMODEL (trains on FILENAME and saves model to SAVEMODEL)\n" +
        "--test FILENAME --model LOADMODEL  (reads model from LOADMODAL and tests on FILENAME)\n" +
        "--model LOADMODEL (reads model and waits for input from stdin)\n\n")
      
      System.exit(1)
    }
   
  
    // train and test on the data again
    if (trainFileName.nonEmpty) { // TRAIN MODEL
      System.err.println("TRAINING A MODEL!")
      val ec = new Classifier(new MyModel)
      val trainCorpus = Corpus.readFromFile(new File(trainFileName),ec.getModel,take)
      ec.getModel.buildVocabularyMapper(trainCorpus)
      ec.getModel.buildLabelMapper(trainCorpus)
      ec.getModel.fixVoc

      ec.getModel.train(trainCorpus)
      System.err.println("Clean Memory")
      System.gc
      System.err.println("Done.")

      System.err.println("Results on train corpus " + trainFileName)
      ec.getModel.predict(trainCorpus)
      val resubresult = trainCorpus.evaluate
      resubresult.printResults(System.err)
      
      // if training was performed and a modelFile was provided, save the model
      if (modelFileName.nonEmpty) {
        ec.getModel.save(modelFileName)
      }

      // test
      if (testFileName.nonEmpty) {
        val testCorpus = Corpus.readFromFile(new File(testFileName),ec.getModel)
        System.err.println("Results on test corpus " + testFileName)
        ec.getModel.predict(testCorpus)
        val result = testCorpus.evaluate
        result.printResults(System.err)
      }
    }
    else if (modelFileName.nonEmpty) { // APPLY MODEL
      System.err.println("READING AND APPLYING A MODEL!")
      // read model
      val em = MyModel.load(modelFileName)
      val ec = new Classifier(em)

      // Input could be read from Stdin or from a test file, let's start with a test file, evaluate, and show the output
      if (testFileName.nonEmpty) {
        val testCorpus = Corpus.readFromFile(new File(testFileName),ec.getModel,take)
        System.err.println("Results on test corpus " + testFileName)
        ec.getModel.predict(testCorpus)
        val result = testCorpus.evaluate
        result.printResults(System.err)
        // print results
        testCorpus.printResults(System.out)
      } else { // read from standard in
        System.err.println("Reading plain text from standard in in format ID[TAB]Text. If tab is missing, the whole line is used as text.")
        for (ln <- scala.io.Source.stdin.getLines) {
          if (ln.contains("\t")) {
            val s = ln.split("\t")
            ec.getModel.predictText(s(1),s(0),System.out)
          } else
            ec.getModel.predictText(ln,"",System.out)
        }
      }
    }
  }
}
