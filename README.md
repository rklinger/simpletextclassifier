# This is a small program encapsulating a liblinear implementation for easy command line use to generate baseline results in text classification. #

## How to use? ##

### Compile ###
Please have Maven installed on your computer. Then:

```
mvn compile assembly:single
```
This will download the relevant libraries and package a jar file. This can be executed as follows.

### Train a model ###

This will train a model on `train.csv` and test it on `test.csv`. In addition, it will save a model file in `twitter.em`. The `--test` command can be omitted as can the `--model`.

```
./bin/run.sh --train data/train.csv --test data/test.csv --model model/twitter.em
```

### Test a model ###

Using the model trained in the previous step, you can test this on different files. For instance, try

```
./bin/run.sh --test data/tales.csv --model model/twitter.em
```

### Apply a model to standard in or interactively ###
```
./bin/run.sh --model model/twitter.em
```

If you have questions, please contact me at `klinger@ims.uni-stuttgart.de`.